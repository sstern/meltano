---
metaTitle: Meltano Data Sources
description: Meltano Extractors connect to external services to extract for analysis.
sidebarDepth: 2
---

# Data Sources

Hosted Meltano accounts currently support extracting data from:

* [Facebook Ads](/plugins/extractors/facebook.html)
* [Google Ads](/plugins/extractors/adwords.html)
* [Google Analytics](/plugins/extractors/google-analytics.html)
* [Salesforce](/plugins/extractors/salesforce.html)
* [Stripe](/plugins/extractors/stripe.html)

::: tip
If you'd like to use Meltano with a different data source than those listed here, you can consider [self-hosting Meltano](/developer-tools/self-hosted-installation.md). Self-hosted installations require more manual set up, but support [additional data sources](/plugins/extractors/) and give you the option to [create custom extractors](/tutorials/create-a-custom-extractor.html) to pull in data from arbitrary data sources.
:::
