---
home: true
heroImage: /meltano-logo.svg
primaryAction:
  text: Try It Out For Free!
  link: https://meltano.typeform.com/to/NJPwxv
secondaryAction:
  text: Live Demo
  link: https://meltano.meltanodata.com/dashboards
metaTitle: "Meltano: your startup sales funnel, analyzed."
description: Meltano provides a single set of dashboards and reports for the entire marketing sales funnel and customer journey.
---
